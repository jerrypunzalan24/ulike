import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HostnameProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HostnameProvider {
  hostname: string = "https://ulikebackend.000webhostapp.com/"
  // hostname : String = "http://localhost/ulike-backend/public/";
  constructor(public http: HttpClient) {
    console.log('Hello HostnameProvider Provider');
  }
  getHostname(): String{
    return this.hostname
  }

}
