import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ToastController} from 'ionic-angular';
import {StorageServiceProvider} from '../../providers/storage-service/storage-service';
import {HttpClient} from '@angular/common/http';
import {LoginPage} from '../login/login';
import {HostnameProvider} from '../../providers/hostname/hostname';
import { AboutPage } from '../about/about';

/**
 * Generated class for the RetakePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retake',
  templateUrl: 'retake.html',
})
export class RetakePage {
  answers = []
  questions : any
  constructor(public toastController : ToastController, public navCtrl: NavController, public navParams: NavParams, private http : HttpClient, private storageService : StorageServiceProvider, private h : HostnameProvider) {
    this.http.get(this.h.getHostname() + "getquestions").subscribe(res => {
      this.questions = res      
    }, err =>{
      console.log("error", err)
    })
  }
  submit(){
    this.http.get(this.h.getHostname() + `retake/${this.storageService.get("account_data").account_id}/${this.answers.toString()}`).subscribe(res => {
      this.displayToast("Succcess!!")
      this.navCtrl.pop()
      
    })
  }
  async displayToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    })
    toast.present()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RetakePage');
  }

}
