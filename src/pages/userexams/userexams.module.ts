import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserexamsPage } from './userexams';

@NgModule({
  declarations: [
    UserexamsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserexamsPage),
  ],
})
export class UserexamsPageModule {}
