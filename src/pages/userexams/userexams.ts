import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {StorageServiceProvider} from '../../providers/storage-service/storage-service';
import {HttpClient} from '@angular/common/http';
import {HostnameProvider} from '../../providers/hostname/hostname';
/**
 * Generated class for the UserexamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-userexams',
  templateUrl: 'userexams.html',
})
export class UserexamsPage {
  questions : any
  constructor(public navCtrl: NavController, public navParams: NavParams, private http : HttpClient, private storageService : StorageServiceProvider, private h : HostnameProvider) {
  }

  getExamAnswer(number){
    if(number == 0){
      return "Not at all"
    }
    if(number == 1){
      return "Several days"
    }
    else if(number == 2){
      return "More than half the days"
    }
    else if(number == 3){
      return "Nearly every day"
    }
  }
  ionViewDidLoad() {
    this.http.get(this.h.hostname + `retrieveexams/${this.storageService.get("account_data").account_id}`).subscribe(res =>{
      console.log(res)
      this.questions = res
    },err =>{

    })
    console.log('ionViewDidLoad UserexamsPage');
  }


}
