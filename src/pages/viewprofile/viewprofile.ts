import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from '@angular/common/http';
import {HostnameProvider} from '../../providers/hostname/hostname';
/**
 * Generated class for the ViewprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewprofile',
  templateUrl: 'viewprofile.html',
})
export class ViewprofilePage {
  profileData = {}
  id : number
  picture : any
  constructor(public navCtrl: NavController, public navParams: NavParams, private h : HostnameProvider, private http : HttpClient) {
    this.id = navParams.get("id")
    this.picture = navParams.get("picture")
    this.http.get(h.getHostname() + "retrieveprofile/" + this.id).subscribe((res)=>{
      console.log(res)
      this.profileData = res
    }, err =>{
      console.log("Error, ", err)
    })
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewprofilePage');
  }

}
