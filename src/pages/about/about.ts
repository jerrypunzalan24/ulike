import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import {HttpClient} from '@angular/common/http';
import {StorageServiceProvider} from '../../providers/storage-service/storage-service';
import {HostnameProvider} from '../../providers/hostname/hostname';
import { Geolocation } from '@ionic-native/geolocation';
import {LoginPage} from '../login/login';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  preferences:any = {}
  id: number
  current: number = 0
  preference: number

  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy:number;
  geoAddress: string;
 
  watchLocationUpdates:any; 
  loading:any;
  isWatching:boolean;

  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  constructor(public navCtrl: NavController, private http : HttpClient, private storageService : StorageServiceProvider
    , private geolocation : Geolocation, private nativeGeocoder: NativeGeocoder, public h : HostnameProvider,
    public toastCtrl : ToastController) {
    
  }
  updateLocation(){
    let toast = this.toastCtrl.create({
      message: 'Your device location is not available. Please turn on your GPS.',
      position: 'middle',
      showCloseButton: true,
      closeButtonText: 'OK'
   });
    this.geolocation.getCurrentPosition().then((resp)=>{
      this.geoLatitude = resp.coords.latitude;
      this.geoLongitude = resp.coords.longitude;
      this.geoAccuracy = resp.coords.accuracy;
      this.nativeGeocoder.reverseGeocode(this.geoLatitude, this.geoLongitude, this.geoencoderOptions)
      .then((result : NativeGeocoderReverseResult[])=>{
        this.geoAddress = this.generateAddress(result[0])
        let body = {
          id : this.storageService.get("account_data").account_id
        }
        this.http.get(this.h.getHostname() + `updatelocation/${body.id}/${this.geoAddress}`).subscribe(res=>{
          console.log(res)
        },err => {
          console.log(this.geoAddress)
          console.log("Error: ", err)
        })
      }).catch(error=>{
        console.log(this.geoAddress)
        let body = {
          id : this.storageService.get("account_data").account_id
        }
        this.http.get(this.h.getHostname() + `updatelocation/${body.id}/${this.geoAddress}`).subscribe(res=>{
          console.log(res)
        },err => {
          console.log(this.geoAddress)
          console.log("Error: ", err)
        })
        console.log("Error getting location 1", JSON.stringify(error))
      })
    }).catch(error=>{
      console.log("Error getting location 2", JSON.stringify(error))
      toast.present()
      this.http.get(this.h.getHostname() + `logout/${this.storageService.get("account_data").account_id}`).subscribe(res=>{
        this.navCtrl.setRoot(LoginPage)
      })
    })
  }
  async displayToast(msg){
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    })
    toast.present()
  }
  generateAddress(addressObj){
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if(obj[val].length)
      address += obj[val]+', ';
    }
  return address.slice(0, -2);
}
ionViewDidLoad(){
  this.updateLocation()
}
  ionViewWillEnter(){
    console.log(this.preferences)
    this.current = 0
    this.id = this.storageService.get("account_data").account_id
    this.preference = this.storageService.get("account_data").preference
    this.http.get(this.h.hostname + "getpreference/" + this.id + "/" + this.preference).subscribe(res => {
      console.log("Success!")
      this.preferences = res
      console.log(res)
    }, err => {
      console.log(err)
    })

  }
  like(){
    let body = {
      id: this.id,
      other_id : this.preferences[this.current].account_id,
      like : true
    }
    this.http.get(this.h.hostname + `match/${body.id}/${body.other_id}/0`).subscribe(res => {
      console.log("Liked")
      console.log(res)
      if(res['matches']!==undefined){
        this.displayToast("You matched!")
      }
      this.current++
    }, err => {
      console.log("Check your internet connection ", err)
    })
  }
  dislike(){
    let body = {
      id : this.id,
      other_id : this.preferences[this.current].account_id,
      dislike : true
    }
    this.http.get(this.h.hostname + `match/${body.id}/${body.other_id}/1`).subscribe( res => {
      console.log("Disliked")
      this.current++
    }, err => {
      console.log("Check your internet connection ", err)
    })
  }
}
