import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import {StorageServiceProvider} from '../providers/storage-service/storage-service';
import {HttpClient} from '@angular/common/http';
import {HostnameProvider} from '../providers/hostname/hostname';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any 
  // rootPage: any = GalleryPage
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storageService : StorageServiceProvider,
    private http : HttpClient, private h : HostnameProvider) {
    platform.ready().then(() => {
      if(this.storageService.get("account_data")!== undefined){
        this.http.get(this.h.getHostname() + "visit/" + this.storageService.get("account_data").account_id).subscribe(res => {
          this.rootPage = TabsPage
        })
      }else{
        this.rootPage = LoginPage
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
